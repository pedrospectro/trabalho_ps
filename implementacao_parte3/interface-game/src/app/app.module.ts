import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { ProfessorComponent } from './professor/professor.component';
import { JogadorComponent } from './jogador/jogador.component';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { HomeComponent } from './home/home.component';
import { EtapaComponent } from './etapa/etapa.component';
import { ProblemaComponent } from './problema/problema.component';
import { ApiService } from './api.service';
import { CicloComponent } from './ciclo/ciclo.component';
import { OrcamentoComponent } from './orcamento/orcamento.component';
import { CronogramaComponent } from './cronograma/cronograma.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfessorComponent,
    JogadorComponent,
    HomeComponent,
    EtapaComponent,
    ProblemaComponent,
    CicloComponent,
    OrcamentoComponent,
    CronogramaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
