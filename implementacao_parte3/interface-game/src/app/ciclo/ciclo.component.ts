import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-ciclo',
  templateUrl: './ciclo.component.html',
  styleUrls: ['./ciclo.component.css']
})
export class CicloComponent implements OnInit {
  ciclos:any;
  ciclo_selected:any;
  constructor(private  _apiService: ApiService, private router: Router) {
    this.ciclo_selected = null;
    this.ciclos = null;
    this.getProblemaCiclos();
  }

  ngOnInit() {
  }

  getProblemaCiclos(){
    this._apiService.getProblemaCiclos().subscribe(
      data => {
        this.ciclos = data;
      },
      err => {
        alert("Não foi possīvel achar os Ciclos de desenvolvimento - " + err.error.msg);
      }
    );
  }

  escolheCiclo(){
    if(this.ciclo_selected==null)
      alert("Selecione um ciclo para continuar");
    else{
      this._apiService.escolheCiclo(this.ciclo_selected).subscribe(
        data => {
          alert("Ciclo Atualizado");
          this.router.navigateByUrl("/jogador");
        },
        err => {
          alert("Não foi possīvel atualizar o ciclo de desenvolvimento - " + err.error);
        }
      );
    }
  }
}
