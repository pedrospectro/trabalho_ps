import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  etapa: any;
  objetivo: any;
  apiUrl:string;

  constructor(private http:HttpClient) {
    this.apiUrl = 'http://localhost:3000/';
    this.etapa=[];
    this.objetivo=[];
  }

  cadastrarProblema(descricao : string) {
    return this.http.post(
      this.apiUrl+'problema', {descricao: descricao}
    )
  }

  cadastrarEtapa(nome: string) {
    return this.http.post(
      this.apiUrl+'etapa', {nome: nome}
    )
  }

  cadastrarObjetivo(etapa:any, lista_especificacao: any, lista_custo: any) {
    return this.http.post(
      this.apiUrl+'etapa/'+etapa.id+'/objetivo', {
        lista_especificacao: lista_especificacao,
        lista_custo: lista_custo
      }
    )
  }

  getProblema() {
    return this.http.get(
      this.apiUrl+'problema'
    )
  }

  getProblemaCiclos() {
    return this.http.get(
      this.apiUrl+'problema/ciclo'
    )
  }

  escolheCiclo(ciclo_id :int) {
    return this.http.post(
      this.apiUrl+'problema/ciclo', {ciclo_id: ciclo_id}
    )
  }

  montarCronograma(numero_ciclos: int) {
    return this.http.post(
      this.apiUrl+'problema/cronograma', {numero_ciclos: numero_ciclos}
    )
  }

  montarOrcamento(valor: double) {
    return this.http.post(
      this.apiUrl+'problema/orcamento', {valor: valor}
    )
  }

  getSaldo() {
    return this.http.get(
      this.apiUrl+'jogador'
    )
  }

  leObjetivo(etapa:any) {
    return this.http.get(
      this.apiUrl+'objetivo/'+etapa.id
    )
  }

  montarListaAtividades(etapa_id:int , lista_atividades:any) {
    return this.http.post(
      this.apiUrl+'etapa/'+etapa_id+'/atividade', {lista_atividades:lista_atividades}
    )
  }

  getDevTipo() {
    return this.http.get(
      this.apiUrl+'dev'
    )
  }

  contrataDevs(devs:any){
    return this.http.post(
      this.apiUrl+'dev', {devs: devs}
    )
  }

  getDevContratados() {
    return this.http.get(
      this.apiUrl+'dev/contratados'
    )
  }

  getRecursos() {
    return this.http.get(
      this.apiUrl+'recursos'
    )
  }

  alocarRecurso(dev_id, recurso_id) {
    return this.http.post(
      this.apiUrl+'dev/'+dev_id+'/recursos/'+recurso_id, {}
    )
  }

  analisarCustoEtapa(etapa_id: any){
    return this.http.get(
      this.apiUrl+'etapa/'+etapa_id+'/custo/'
    )
  }

  calculaSaldoPosEtapa(etapa_id: any){
    return this.http.get(
      this.apiUrl+'etapa/'+etapa_id+'/calcula_saldo_pos_etapa/'
    )
  }

  rodarPrimeiraEtapa(){
    return this.http.get(
      this.apiUrl+'etapa/rodar_primeira_etapa/'
    )
  }

  modificaAtividade(atividade_id:any,data :any){
    return this.http.post(
      this.apiUrl+'atividade/'+atividade_id+'/modifica_atividades', {descricao: data}
    )
  }

  alocaDesalocaRecursos(dev_id:any){
    return this.http.post(
      this.apiUrl+'dev/'+dev_id+'/aloca_desaloca_recursos', {}
    )
  }

  rodarProximaEtapa(){
    return this.http.get(
      this.apiUrl+'etapa/rodar_proxima_etapa/'
    )
  }

  verificaFimEtapa(etapa_id:any){
    return this.http.get(
      this.apiUrl+'etapa/'+etapa_id+'/verifica_fim_etapa/'
    )
  }
}
