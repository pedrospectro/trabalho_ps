import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-jogador',
  templateUrl: './jogador.component.html',
  styleUrls: ['./jogador.component.css']
})
export class JogadorComponent implements OnInit {
  problema_descricao: string;
  etapa: any;
  objetivo_list: any;
  ciclo:any;
  cronograma:any;
  orcamento:any;
  objetivo: any;
  atividade: string;
  lista_atividades: any;
  atividades_enviadas: int;
  devs: any;
  devs_contratados: any;
  projetista_selected: any;
  db_selected: any;
  test_selected: any;
  programador_selected: any;
  projetista_selected_new: any;
  db_selected_new: any;
  test_selected_new: any;
  programador_selected_new: any;
  recs:any;
  recs_disponiveis:any;
  projetista_rec: any;
  programador_rec: any;
  db_rec: any;
  test_rec: any;
  projetista_rec_selected: any;
  programador_rec_selected: any;
  db_rec_selected: any;
  test_rec_selected: any;
  analise_custo:any;
  primeira_etapa_rodada:any;
  primeira_etapa_concluida:any;
  escolhe_atividade: any;
  escolhe_recurso: any;
  atividade_escolhida:any
  nova_descricao: any;
  dev_escolhido:any;
  resultado_jogo:any;
  devs_contratados_alterar:any;
  fim_etapa:any;
  ultimas_atividades_enviadas:any;
  db_realoc:any
  test_realoc:any
  prog_realoc:any
  proj_realoc:any
  projetista_rec_selected_new: any;
  programador_rec_selected_new: any;
  db_rec_selected_new: any;
  test_rec_selected_new: any;
  jogador: any;
  constructor(private  _apiService: ApiService, private router: Router) {
    this.atividades_enviadas=false;
    this.lista_atividades = [];
    this.getProblema();
  }

  ngOnInit() {
  }

  getProblema(){
    this._apiService.getProblema().subscribe(
      data => {
        this.problema_descricao = data.problema.descricao;
        this.etapa = data.etapa;
        this.atividades_enviadas=data.atividades_enviadas;
        this.jogador = data.jogador;
        this.objetivo_list = data.objetivos;
        this.ciclo = data.ciclo;
        this.cronograma = data.cronograma;
        this.orcamento = data.orcamento;
        this.devs_contratados = data.devs_contratados;
        if(this.devs_contratados.projetista==null && this.devs_contratados.programador==null && this.devs_contratados.test==null && this.devs_contratados.db==null)
          this.devs_contratados=null;
        this.recs = data.recs;
        this.projetista_selected = data.projetista_selected;
        this.proj_realoc = data.projetista_selected;
        this.db_selected = data.db_selected;
        this.db_realoc = data.db_selected;
        this.test_selected = data.test_selected;
        this.test_realoc = data.test_selected;
        this.programador_selected = data.programador_selected;
        this.prog_realoc = data.programador_selected;
        this.primeira_etapa_rodada = data.primeira_etapa_rodada;
        this.primeira_etapa_concluida = data.primeira_etapa_concluida;
        this.ultimas_atividades_enviadas = data.ultimas_atividades_enviadas;
      },
      err => {
        alert("Não foi possīvel achar o Problema - " + err.error.msg);
      }
    );
  }

  leObjetivo(){
    this._apiService.leObjetivo(this.etapa).subscribe(
      data => {
        this.objetivo = data;
        for(var i=0;i<this.objetivo.length;i++)
          alert("Especificacao do Objetivo = " + this.objetivo[i].especificacao + ", Custo = " + this.objetivo[i].custo_etapa);
      },
      err => {
        alert("Não foi possīvel achar o Objetivo -" + err.error.msg);
      }
    );
  }

  adicionarNovaAtividade(){
    this.lista_atividades[this.lista_atividades.length]=this.atividade;
    this.atividade = null;
    if(this.lista_atividades.length==this.objetivo.length){
      alert("Enviando Atividades");
      this.montarListaAtividades();
    }
  }

  montarListaAtividades(){
    this._apiService.montarListaAtividades(this.etapa.id,this.lista_atividades).subscribe(
      data => {
        alert("Atividades Associadas");
        this.atividades_enviadas = data;
      },
      err => {
        alert("Não foi associar atividades com os objetivos da etapa -" + err.error.msg);
      }
    );
  }

  getDevTipo(){
    this._apiService.getDevTipo().subscribe(
      data => {
        this.devs = data;
      },
      err => {
        alert("Não foi associar achar os devs -" + err.error.msg);
      }
    );
  }

  contrataDevs(){
    this._apiService.contrataDevs(
      {
        "projetista": this.projetista_selected,
        "programador": this.programador_selected,
        "test": this.test_selected,
        "db": this.db_selected
      }
    ).subscribe(
      data => {
        alert("Devs Contratados");
        this.getDevContratados();
      },
      err => {
        alert("Não foi contratar os devs -" + err.error.msg);
      }
    );
  }

  contrataDevs2(){
    this._apiService.contrataDevs(
      {
        "projetista": this.projetista_selected ? this.projetista_selected : null,
        "programador": this.programador_selected ? this.programador_selected : null,
        "test": this.test_selected ? this.test_selected : null,
        "db": this.db_selected ? this.db_selected : null
      }
    ).subscribe(
      data => {
        alert("Devs Contratados");
        this.getDevContratados();
      },
      err => {
        alert("Não foi contratar os devs -" + err.error.msg);
      }
    );
  }

  getDevContratados(){
    this._apiService.getDevContratados().subscribe(
      data => {
        this.devs_contratados = data;
      },
      err => {
        alert("Não foi capturar os devs contratados -" + err.error.msg);
      }
    );
  }

  getRecursos(){
    this._apiService.getRecursos().subscribe(
      data => {
        this.recs_disponiveis = data.recursos;
        this.projetista_rec = data.projetista_rec_selected;
        this.programador_rec = data.programador_rec_selected;
        this.db_rec = data.db_rec_selected;
        this.test_rec = data.test_rec_selected;
      },
      err => {
        alert("Não foi possivel achar os recursos -" + err.error.msg);
      }
    );
  }

  alocarRecurso(dev_tipo: any){
    var dev_id = 0;
    var rec_id = 0;
    if(dev_tipo == 'projetista'){
      dev_id = this.projetista_selected.id;
      rec_id = this.projetista_rec_selected;
      this.projetista_rec = this.projetista_rec_selected;
    }
    else if(dev_tipo == 'programador'){
      dev_id = this.programador_selected.id;
      rec_id = this.programador_rec_selected;
      this.programador_rec = this.programador_rec_selected;
    }
    else if(dev_tipo == 'test'){
      dev_id = this.test_selected.id;
      rec_id = this.test_rec_selected;
      this.test_rec = this.test_rec_selected;
    }
    else if(dev_tipo == 'db'){
      dev_id = this.db_selected.id;
      rec_id = this.db_rec_selected;
      this.db_rec = this.db_rec_selected;
    }
    this._apiService.alocarRecurso(dev_id, rec_id).subscribe(
      data => {
        this.getRecursos();
      },
      err => {
        alert("Não foi possivel alocar o recurso-" + err.error.msg);
      }
    );
  }

  alocarRecursoNovaEtapa(dev_tipo: any){
    var dev_id = 0;
    var rec_id = 0;
    if(dev_tipo == 'projetista'){
      dev_id = this.projetista_selected;
      rec_id = this.projetista_rec_selected_new;
      this.projetista_rec = this.projetista_rec_selected_new;
    }
    else if(dev_tipo == 'programador'){
      dev_id = this.programador_selected;
      rec_id = this.programador_rec_selected_new;
      this.programador_rec = this.programador_rec_selected_new;
    }
    else if(dev_tipo == 'test'){
      dev_id = this.test_selected;
      rec_id = this.test_rec_selected_new;
      this.test_rec = this.test_rec_selected_new;
    }
    else if(dev_tipo == 'db'){
      dev_id = this.db_selected;
      rec_id = this.db_rec_selected_new;
      this.db_rec = this.db_rec_selected_new;
    }
    this.contrataDevs2();
    this._apiService.alocarRecurso(dev_id, rec_id).subscribe(
      data => {
        this.getRecursos();
      },
      err => {
        alert("Não foi possivel alocar o recurso-" + err.error.msg);
      }
    );
  }

  analisarCustoEtapa(){
    this._apiService.analisarCustoEtapa(this.etapa.id).subscribe(
      data => {
        this.analise_custo = data;
        alert("Esta Etapa tera um custo de "+this.analise_custo+" por ciclo");
      },
      err => {
        alert("Não foi possivel Analisar o Custo da primeira Etapa");
      }
    );
  }

  calculaSaldoPosEtapa(){
    this._apiService.calculaSaldoPosEtapa(this.etapa.id).subscribe(
      data => {
        alert("Depois desta etapa seu saldo será de "+data.saldo_pos_etapa);
      },
      err => {
        alert("Não foi possivel Calcular o custo pos etapa");
      }
    );
  }

  rodarPrimeiraEtapa(){
    alert("Rodando primeira etapa");
    this.calculaSaldoPosEtapa(); 
    this._apiService.rodarPrimeiraEtapa().subscribe(
      data => {
        alert("Etapa Rodada com sucesso, Mostrando Resultados");
        var nome = data.etapa.nome;
        var list_status = data.lista_status;
        var saldo = data.saldo;
        var resultado = "Saldo Atual: "+saldo+", Resultado da etapa "+nome+": ";
        var concluida=true;
        for(var i=0; i<list_status.length ; i++){
          if(list_status[i]==false)
            concluida=false;
          resultado = resultado+" atividade "+i+" = " + ( list_status[i]==true? "Finalizada, " : "Em andamento, ");
        }
        alert(resultado);
        this.primeira_etapa_rodada=true;
        this.primeira_etapa_concluida=concluida;
        if(this.primeira_etapa_concluida)
          this.router.navigateByUrl("/jogador");
      },
      err => {
        alert("Não foi possivel rodar a primeira etapa");
      }
    );
  }

  escolheAtividade(){
    this.escolhe_atividade=true;
    this.escolhe_recurso=false;
    this.lista_atividades=[];
    this.getProblema();
  }

  escolheRecurso(){
    this.escolhe_recurso=true;
    this.escolhe_atividade=false;
    this.devs_contratados_alterar = [];
    if(this.devs_contratados.db)
      this.devs_contratados_alterar.push(this.devs_contratados.db);
    if(this.devs_contratados.projetista)
      this.devs_contratados_alterar.push(this.devs_contratados.projetista);
    if(this.devs_contratados.programador)
      this.devs_contratados_alterar.push(this.devs_contratados.programador);
    if(this.devs_contratados.test)
      this.devs_contratados_alterar.push(this.devs_contratados.test);
      
    
    this.getDevTipo();
    this.getRecursos();
  }

  modificaAtividades(){
    this.escolhe_atividade=false;
    this._apiService.modificaAtividade(this.atividade_escolhida,this.nova_descricao).subscribe(
      data => {
        alert("Atividades Modificadas");
        this.router.navigateByUrl("/jogador");
      },
      err => {
        alert("Não foi possivel ModificarAtividades");
      }
    );
  }

  alocaDesalocaRecursos(){
    this.escolhe_recurso=false;
    this._apiService.alocaDesalocaRecursos(this.dev_escolhido).subscribe(
      data => {
        alert("Dev e recurso desalocados");
        location.reload();
      },
      err => {
        alert("Não foi possivel desalocar o dev e seu recurso");
      }
    );
  }

  verificaFimEtapa(){
    this._apiService.verificaFimEtapa(this.etapa.id).subscribe(
      data => {
        this.fim_etapa = data.resultado;
        if(this.fim_etapa==-1)
          alert("Voce estourou o orcamento, vc perdeu");
        else if(this.fim_etapa==0)
          alert("Etapa nao finalizada, jogo continua");
        else if(this.fim_etapa==1)
          alert("Etapa Concluida");
        else if(this.fim_etapa==2)
          alert("Etapa Concluida, Jogo finalizado, parabens");
      },
      err => {
        alert("Não foi possivel verificar o fim da etapa");
      }
    );
  }

  rodarProximaEtapa(){
    alert("Rodando proxima etapa");
    this.calculaSaldoPosEtapa(); 
    this._apiService.rodarProximaEtapa().subscribe(
      data => {
        alert("Etapa Rodada com sucesso, Mostrando Resultados");
        var nome = data.etapa.nome;
        var list_status = data.lista_status;
        var saldo = data.saldo;
        var resultado = "Saldo Atual: "+saldo+", Resultado da etapa "+nome+": ";
        var concluida=true;
        for(var i=0; i<list_status.length ; i++){
          if(list_status[i]==false)
            concluida=false;
          resultado = resultado+" atividade "+i+" = " + ( list_status[i]==true? "Finalizada, " : "Em andamento, ");
        }
        alert(resultado);
        this.verificaFimEtapa();
      },
      err => {
        alert("Não foi possivel rodar a proxima etapa");
      }
    );
  }


}
