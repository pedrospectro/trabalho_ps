import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-etapa',
  templateUrl: './etapa.component.html',
  styleUrls: ['./etapa.component.css']
})
export class EtapaComponent implements OnInit {
  nome: string;
  especificacao: string;
  custo: number;
  lista_especificacao: any;
  lista_custo: any;

  constructor(private  _apiService: ApiService) {
    this.custo = 0.0;
    this.lista_especificacao = [];
    this.lista_custo = [];
  }

  ngOnInit() {
  }

  novaEspecificacao(){
    this.lista_especificacao.push(this.especificacao);
    this.lista_custo.push(this.custo);
    this.especificacao = "";
    this.custo = 0;
  }

  cadastrarEtapas(){
    this.novaEspecificacao();
    this._apiService.cadastrarEtapa(
      this.nome
    ).subscribe(
      data => {
        this._apiService.cadastrarObjetivo(
          data,
          this.lista_especificacao,
          this.lista_custo
        ).subscribe(
          data => {
            alert("Etapas Cadastradas com sucesso");
          },
          err => {
            alert("Não foi possīvel cadastrar as etapas");
          }
        );
      },
      err => {
        alert("Não foi possīvel cadastrar as etapas - " + err.error.msg);
      }
    );
  }

}
