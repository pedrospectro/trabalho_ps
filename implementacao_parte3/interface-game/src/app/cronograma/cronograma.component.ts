import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-cronograma',
  templateUrl: './cronograma.component.html',
  styleUrls: ['./cronograma.component.css']
})
export class CronogramaComponent implements OnInit {
  numero_ciclos:int;
  constructor(private  _apiService: ApiService, private router: Router) { }

  ngOnInit() {
  }

  montarCronograma(){
    this._apiService.montarCronograma(this.numero_ciclos).subscribe(
      data => {
        alert("Conograma criado");
        this.router.navigateByUrl("/jogador");
      },
      err => {
        alert("Não foi possīvel montar o cronograma - " + err.error.msg);
      }
    );
  }
}
