import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-problema',
  templateUrl: './problema.component.html',
  styleUrls: ['./problema.component.css'],
  providers:[ApiService]
})
export class ProblemaComponent implements OnInit {
  descricao: string;
  constructor(private  _apiService: ApiService) { }
  ngOnInit() {
  }

  cadastrarProblema(){
    this._apiService.cadastrarProblema(this.descricao).subscribe(
      data => {
        alert("Problema Cadastrado com sucesso");
      },
      err => {
        alert("Não foi possīvel cadastrar o Problema");
      }
    );
  }
}
