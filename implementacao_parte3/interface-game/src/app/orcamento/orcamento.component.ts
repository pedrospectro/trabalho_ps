import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-orcamento',
  templateUrl: './orcamento.component.html',
  styleUrls: ['./orcamento.component.css']
})
export class OrcamentoComponent implements OnInit {
  jogador:any;
  orcamento:any;
  valor:double;
  constructor(private  _apiService: ApiService, private router: Router) {
    this.getSaldo();
  }

  ngOnInit() {
  }

  getSaldo(){
    this._apiService.getSaldo().subscribe(
      data => {
        this.jogador = data;
      },
      err => {
        alert("Não foi possīvel achar o Jogador ");
      }
    );
  }

  montarOrcamento(){
    this._apiService.montarOrcamento(this.valor).subscribe(
      data => {
        this.orcamento = data;
        alert("Orcamento criado com sucesso");
        this.router.navigateByUrl("/jogador");
      },
      err => {
        alert("Não foi possīvel criar o orcamento -" + err.error.msg);
      }
    );  
  }
}
