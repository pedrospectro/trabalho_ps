import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../home/home.component'

import { ProfessorComponent } from '../professor/professor.component'
import { ProblemaComponent } from '../problema/problema.component'
import { EtapaComponent } from '../etapa/etapa.component'

import { JogadorComponent } from '../jogador/jogador.component'
import { CicloComponent } from '../ciclo/ciclo.component'
import { OrcamentoComponent } from '../orcamento/orcamento.component'
import { CronogramaComponent } from '../cronograma/cronograma.component'


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },
    {
      path: 'jogador',
      component: JogadorComponent,
    },
    {
      path: 'jogador/ciclo',
      component: CicloComponent,
    },
    {
      path: 'jogador/orcamento',
      component: OrcamentoComponent,
    },
    {
      path: 'jogador/cronograma',
      component: CronogramaComponent,
    },
    {
      path: 'professor',
      component: ProfessorComponent,
    },
    {
        path: 'professor/problema',
        component: ProblemaComponent,
    },
    {
        path: 'professor/etapa',
        component: EtapaComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }