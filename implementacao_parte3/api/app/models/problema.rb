class Problema < ApplicationRecord
  belongs_to :professor
  has_many :etapas
  has_one :orcamento
  has_one :cronograma
  has_one :ciclo

  def atualiza_ciclo(ciclo_id)
    ciclo = Ciclo.find(ciclo_id)
    ciclo.update(problema_id: self.id)
    ciclo
  end

  def get_ciclos_disponiveis
    Ciclo.all
  end

  def self.criar_cronograma(numero_ciclos)
    Cronograma.criar_cronograma(
      numero_ciclos,
      Problema.last.id
    )
  end

  def self.criar_orcamento(valor)
    Orcamento.criar_orcamento(
      valor,
      Problema.last.id
    )
  end
end
