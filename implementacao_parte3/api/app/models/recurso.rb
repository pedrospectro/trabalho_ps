class Recurso < ApplicationRecord

  def self.associa_recurso(dev_id, rec_id)
    Recurso.find(rec_id).update(dev_id: dev_id)
  end

  def self.get_recursos_alocados
    recursos = Recurso.where("dev_id is not NULL")
    if recursos.count != Dev.where(contratado: true).count
      return nil
    else
      return recursos
    end
  end
end
