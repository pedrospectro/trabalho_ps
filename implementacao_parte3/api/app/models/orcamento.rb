class Orcamento < ApplicationRecord
  belongs_to :problema

  def self.criar_orcamento(valor, problema_id)
    Orcamento.create(
      valor: valor,
      problema_id: problema_id
    )
  end
end
