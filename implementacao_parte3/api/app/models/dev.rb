class Dev < ApplicationRecord
  has_one :recurso

  def self.select_tipo
    {
      db: Dev.where(tipo: "db"),
      projetista: Dev.where(tipo: "projetista"),
      programador: Dev.where(tipo: "programador"),
      test: Dev.where(tipo: "test")
    }
  end

  def self.contrata_devs(projetista,db,programador,tester)
    {
      projetista: ( projetista ? Dev.find(projetista).update(contratado:true) : nil),
      db: ( db ? Dev.find(db).update(contratado:true) : nil ),
      programador: ( programador ? Dev.find(programador).update(contratado:true) : nil ),
      test: ( tester ? Dev.find(tester).update(contratado:true) : nil )
    }
  end

  def self.contratados
    data = {
      projetista: Dev.where(tipo: "projetista", contratado: true).first,
      db: Dev.where(tipo: "db", contratado: true).first,
      programador: Dev.where(tipo: "programador", contratado: true).first,
      test: Dev.where(tipo: "test", contratado: true).first
    }

    if Etapa.first.finalizada==false && (data[:projetista].nil? || data[:db].nil? || data[:programador].nil? || data[:test].nil?)
      return nil
    else
      return data
    end
  end

  def self.aloca_recurso(dev_id, recurso_id)
    Recurso.associa_recurso(dev_id,recurso_id)
  end

  def self.get_custo_dev
    Dev.where(contratado: true).sum(:salario_ciclo) + Recurso.where("dev_id is not NULL").sum(:custo_ciclo)
  end

  def self.simula_dev_recurso()
    devs = Dev.where(contratado: true)
    resultado = 0
    for dev in devs
      if dev.numero_ciclo_indisponivel>0
        dev.update(numero_ciclo_indisponivel: dev.numero_ciclo_indisponivel-1)
      else
        dev_treinamento_p = rand(1..100)>70
        dev_problemas_p = rand(1..100)>70
        recurso_p = rand(1..100)>90
        atividade_ciclo = 0
        if not recurso_p
          atividade_ciclo = dev.atividade_ciclo
          if dev_treinamento_p
            atividade_ciclo = atividade_ciclo/2.0
          end
          if dev_problemas_p 
            atividade_ciclo = atividade_ciclo*70.0/100.0
          end
        end
        resultado = resultado+atividade_ciclo
        dev_licensa_p = rand(1..100)
        if dev_licensa_p>90
          dev.update(numero_ciclo_indisponivel:7)
        end
      end
    end
    return resultado>=1
  end

  def self.get_devs_recursos
  end

  def remove_dev_e_recursos
    rec = recurso
    self.update(contratado: false)
    self.update(numero_ciclo_indisponivel: 0)
    rec.update(dev_id: nil) if rec
    return self
  end
end
