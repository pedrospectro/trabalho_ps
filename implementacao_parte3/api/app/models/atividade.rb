class Atividade < ApplicationRecord
  belongs_to :jogador
  belongs_to :etapa

  def self.get_primeira_etapa_atividades
    Etapa.first.atividades.where(status:0)
  end

  def self.get_proxima_etapa_atividades
    Etapa.where(finalizada:nil).first.atividades.where(status:0)
  end

  def simula
    Dev.simula_dev_recurso
  end

  def finaliza_atividade
    self.update(status:1)
  end
end
