class Jogador < ApplicationRecord
  has_many :atividades
  has_many :devs

  def self.atualiza_saldo(valor)
    Jogador.first.update(saldo: Jogador.first.saldo - valor)
    Jogador.first.saldo
  end

  def multa(etapa_id)
    Jogador.first.update(saldo: Jogador.first.saldo - (Etapa.get_orcamento * 5.0/100.0))
  end

  def self.get_jogador
    Jogador.first
  end
end
