class Cronograma < ApplicationRecord
  belongs_to :problema

  def self.criar_cronograma(numero_ciclos, problema_id)
    Cronograma.create(
      numero_ciclos: numero_ciclos,
      problema_id: problema_id
    )
  end
end
