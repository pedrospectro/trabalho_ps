class Objetivo < ApplicationRecord
  belongs_to :etapa

  def self.criar_objetivos(etapa, lista_especificacao, lista_custo)
    i = 0
    lista_especificacao.each do |especificacao|
      etapa.objetivos.create(
        especificacao: especificacao,
        custo_etapa: lista_custo[i]
      )
      i = i + 1
    end
    return etapa.objetivos
  end

  def atualiza_custo(custo)
  end
end
