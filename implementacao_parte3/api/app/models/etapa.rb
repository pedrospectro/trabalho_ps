class Etapa < ApplicationRecord
  belongs_to :problema
  has_many :atividades
  has_many :objetivos

  def self.analisa_custo(etapa_id)
    Etapa.find(etapa_id).objetivos.sum(:custo_etapa)
  end

  def self.roda_primeira_etapa
    Atividade.get_primeira_etapa_atividades
  end

  def self.roda_proxima_etapa
    Atividade.get_proxima_etapa_atividades
  end


  def self.atividades_primeira_etapa
  end

  def verifica_atividades_finalizadas
    lista_atividades = self.atividades
    lista = []
    for atividade in lista_atividades
      lista.push(atividade.status == 1)
    end
    if atividades.where(status:0).count==0
      self.update(finalizada: true)
    end
    {
      etapa: self,
      lista_status: lista
    }
  end

  def verifica_cronograma
    Jogador.first.update(ciclos_exec: Jogador.first.ciclos_exec+1)
    if Etapa.estourou_cronograma
      Jogador.first.multa(self.id)
    end
    return Jogador.first.saldo
  end

  def self.estourou_cronograma
    Problema.first.cronograma.numero_ciclos < Jogador.first.ciclos_exec
  end

  def self.get_orcamento
    Problema.first.orcamento.valor
  end

  def atividades_concluidas
    self.atividades.where(status:1)
  end

  def self.get_objetivo(etapa_id)
    Etapa.find(etapa_id).objetivos
  end

  def cria_atividade(descricao)
    self.atividades.create({
      status: 0,
      descricao: descricao,
      jogador_id: Jogador.first.id
    })
  end
end
