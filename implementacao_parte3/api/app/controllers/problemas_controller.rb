class ProblemasController < ApplicationController
  def cadastrar_problema
    @json = JSON.parse(request.body.read)
    render status: :ok, json: Problema.create(
      descricao: @json['descricao'],
      professor_id: Professor.first.id
    )
  end

  def get_problema_ciclos
    render status: :ok, json: Problema.last.get_ciclos_disponiveis
  end

  def get_problema
    if Problema.last.nil?
      render status: :not_found, json: { msg: "Problema e etapas devem estar cadastrados" }
    else
      problema = Problema.last
      etapa = problema.etapas.where(finalizada: nil).first
      render status: :ok, json: {
        problema: problema,
        etapa: etapa,
        atividades_enviadas: etapa.objetivos.count==etapa.atividades.count,
        objetivos: etapa.objetivos,
        ciclo: problema.ciclo,
        cronograma: problema.cronograma,
        orcamento: problema.orcamento,
        devs_contratados: Dev.contratados,
        recs: Recurso.get_recursos_alocados,
        projetista_selected: Dev.where(tipo: "projetista", contratado: true).first,
        db_selected: Dev.where(tipo: "db", contratado: true).first,
        programador_selected: Dev.where(tipo: "programador", contratado: true).first,
        test_selected: Dev.where(tipo: "test", contratado: true).first,
        primeira_etapa_rodada: Jogador.first.ciclos_exec>0,
        primeira_etapa_concluida: Etapa.first.finalizada,
        ultimas_atividades_enviadas: Etapa.where(finalizada: nil).first.atividades,
        jogador: Jogador.first
      }
    end
  end

  def escolhe_ciclo
    ciclo = Problema.last.atualiza_ciclo(JSON.parse(request.body.read)["ciclo_id"])
    if ciclo.save
      render status: :ok, json: { Problema: Problema.last, ciclo: ciclo }
    else
      render status: :unprocessable_entity, json: { msg: ciclo.errors }
    end
  end
end
