class ObjetivosController < ApplicationController
  def cadastrar_objetivo
    objetivos = Objetivo.criar_objetivos(
      Etapa.find(params[:id]),
      JSON.parse(request.body.read)['lista_especificacao'],
      JSON.parse(request.body.read)['lista_custo']
    )
    render status: :ok, json: objetivos
  end

  def cadastra_custo(objetivo_id, custo)
  end

  def le_objetivo
    objetivo = Etapa.get_objetivo(params[:id])
    if objetivo.nil?
      render status: :unprocessable_entity, json: { 
        msg: "Objetivo não especificado para esta etapa"
      }
    else
      render status: :ok, json: objetivo
    end
  end
end
