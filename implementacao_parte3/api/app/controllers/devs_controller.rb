class DevsController < ApplicationController
  def get_dev_tipo
    render status: :ok, json: Dev.select_tipo()
  end

  def contrata_devs
    @json = JSON.parse(request.body.read)
    @projetista = @json["devs"]["projetista"]
    @db = @json["devs"]["db"]
    @programador = @json["devs"]["programador"]
    @test = @json["devs"]["test"]
    contratados = Dev.contrata_devs(@projetista,@db,@programador,@test)
    render status: :ok, json: contratados
  end

  def get_contratados
    render status: :ok, json: Dev.contratados
  end

  def get_dev_e_recursos
  end

  def aloca_desaloca_recursos
    dev_id = params[:id]
    dev = Dev.find(dev_id).remove_dev_e_recursos
    if dev
      render status: :ok, json: dev
    else
      render status: :unprocessable_entity
    end
  end
end
