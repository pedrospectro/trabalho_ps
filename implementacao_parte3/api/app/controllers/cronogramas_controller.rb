class CronogramasController < ApplicationController
  def montar_cronograma
    numero_ciclos = JSON.parse(request.body.read)['numero_ciclos']
    cronograma = Problema.criar_cronograma(numero_ciclos)
    if cronograma.save
      render status: :ok, json: cronograma
    else
      render status: :unprocessable_entity, json: { msg: cronograma.errors }
    end
  end
end
