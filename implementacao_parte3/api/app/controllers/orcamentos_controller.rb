class OrcamentosController < ApplicationController
  def montar_orcamento
    valor = JSON.parse(request.body.read)['valor']
    orcamento = Problema.criar_orcamento(valor)
    if orcamento.save
      render status: :ok, json: orcamento
    else
      render status: :unprocessable_entity, json: { msg: orcamento.errors }
    end
  end
end
