class AtividadesController < ApplicationController
  def montar_lista_atividades
    @json = JSON.parse(request.body.read)
    etapa = Etapa.find(params[:id])
    lista_descricao = @json["lista_atividades"]
    atividades = []
    for descricao in lista_descricao
      atividades << etapa.cria_atividade(descricao)
    end
    if atividades.count == etapa.objetivos.count
      render status: :ok, json: atividades
    else
      render status: :unprocessable_entity, json: {msg: "Erro ao criar as atividades"}
    end
  end

  def modifica_atividades
    @descricao = JSON.parse(request.body.read)["descricao"]
    atividade_id = params[:id]
    atividade = Atividade.find(atividade_id).update(descricao: @descricao)
    if atividade
      render status: :ok, json: {atividade: atividade}
    else
      render status: :unprocessable_entity
    end
  end

  def remove_atividade
    atividade_id = params[:id]
  end
end
