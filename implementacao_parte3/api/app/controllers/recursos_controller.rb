class RecursosController < ApplicationController
  def get_recursos
    proj = Dev.where(tipo: "projetista", contratado: true).first
    tester = Dev.where(tipo: "test", contratado: true).first
    db = Dev.where(tipo: "db", contratado: true).first
    programador = Dev.where(tipo: "programador", contratado: true).first
    if Etapa.first.finalizada
      render status: :ok, json: {
        recursos: Recurso.where("dev_id is NULL"),
        projetista_rec_selected: proj ? Recurso.where(dev_id: proj.id).first.id : nil,
        programador_rec_selected: programador ? Recurso.where(dev_id: programador.id).first.id : nil,
        db_rec_selected: db ? Recurso.where(dev_id: db.id).first.id : nil,
        test_rec_selected: tester ? Recurso.where(dev_id: tester.id).first.id : nil
      }
    else
      render status: :ok, json: {
        recursos: Recurso.where("dev_id is NULL"),
        projetista_rec_selected: Recurso.where(dev_id: proj.id).first ? Recurso.where(dev_id: proj.id).first.id : nil,
        programador_rec_selected: Recurso.where(dev_id: programador.id).first ? Recurso.where(dev_id: programador.id).first.id : nil,
        db_rec_selected: Recurso.where(dev_id: db.id).first ? Recurso.where(dev_id: db.id).first.id : nil,
        test_rec_selected: Recurso.where(dev_id: tester.id).first ? Recurso.where(dev_id: tester.id).first.id : nil
      }
    end
  end

  def aloca_recurso
    dev_id = params[:dev_id]
    rec_id = params[:rec_id]
    recurso = Dev.aloca_recurso(dev_id,rec_id)
    if not recurso.nil?
      render status: :ok, json: recurso
    else
      render status: :unprocessable_entity, json: recurso.errors
    end
  end

  def get_recursos_alocados
  end
end
