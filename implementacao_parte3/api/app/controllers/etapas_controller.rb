class EtapasController < ApplicationController
  def cadastrar_etapa
    nome = JSON.parse(request.body.read)['nome']
    if Problema.last.nil?
      render status: :not_found, json: {msg: "Problema nao existe"} 
    else
      etapa = Problema.last.etapas.create(nome: nome)
      render status: :ok, json: etapa
    end
  end

  def analisar_custo_etapa
    render status: :ok, json: Etapa.analisa_custo(params[:id])
  end

  def calcula_saldo_pos_etapa
    etapa_id = params[:id]
    custo_dev = Dev.get_custo_dev
    saldo = Jogador.atualiza_saldo(custo_dev + Etapa.analisa_custo(etapa_id))
    render status: :ok, json: { saldo_pos_etapa: saldo }
  end

  def rodar_primeira_etapa
    lista_atividades = Etapa.roda_primeira_etapa
    for atividade in lista_atividades
      resultado = atividade.simula()
      if resultado
        atividade.finaliza_atividade()
      end
    end
    status = Etapa.first.verifica_atividades_finalizadas()
    saldo = Etapa.first.verifica_cronograma()
    render status: :ok, json: {
      etapa: status[:etapa],
      lista_status: status[:lista_status],
      saldo: saldo
    }
  end

  def verifica_fim_etapa
    etapa_id = params[:id]
    etapa = Etapa.find(etapa_id)
    orcamento = Etapa.get_orcamento
    atividades_concluidas = etapa.atividades_concluidas
    if atividades_concluidas.count < etapa.atividades.count
      resultado = verifica_estouro_orcamento(Jogador.first.saldo, orcamento)
    else
      if etapa_id.to_i == Etapa.last.id
        resultado = 2
      else
        resultado = 1
      end
    end
    render status: :ok, json: { resultado: resultado }
  end

  def rodar_proxima_etapa
    lista_atividades = Etapa.roda_proxima_etapa
    for atividade in lista_atividades
      resultado = atividade.simula()
      if resultado
        atividade.finaliza_atividade()
      end
    end
    saldo = Etapa.where(finalizada:nil).first.verifica_cronograma()
    status = Etapa.where(finalizada:nil).first.verifica_atividades_finalizadas()
    render status: :ok, json: {
      etapa: status[:etapa],
      lista_status: status[:lista_status],
      saldo: saldo
    }
  end

  private
  def verifica_estouro_orcamento(saldo, orcamento)
    if ((100000-saldo)>orcamento)
      return -1
    else
      return 0
    end
  end
end
