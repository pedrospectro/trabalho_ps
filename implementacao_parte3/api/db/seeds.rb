Professor.create(nome: "andrey")
# CICLOS
Ciclo.create(tipo:"waterfall")
Ciclo.create(tipo:"cyclic")
# Jogador
Jogador.create(saldo: 100000, ciclos_exec: 0)

Dev.create(
    tipo: "projetista",
    atividade_ciclo: 0.30,
    salario_ciclo: 100,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "projetista",
    atividade_ciclo:0.60,
    salario_ciclo:400,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "projetista",
    atividade_ciclo:0.80,
    salario_ciclo:700,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "projetista",
    atividade_ciclo:0.90,
    salario_ciclo:1000,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "db",
    atividade_ciclo:0.30,
    salario_ciclo:100,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "db",
    atividade_ciclo:0.60,
    salario_ciclo:400,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "db",
    atividade_ciclo:0.80,
    salario_ciclo:700,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "db",
    atividade_ciclo:0.9,
    salario_ciclo:1000,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "programador",
    atividade_ciclo:0.30,
    salario_ciclo:100,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "programador",
    atividade_ciclo:0.60,
    salario_ciclo:400,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "programador",
    atividade_ciclo:0.80,
    salario_ciclo:700,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "programador",
    atividade_ciclo:0.9,
    salario_ciclo:1000,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "test",
    atividade_ciclo:0.30,
    salario_ciclo:100,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "test",
    atividade_ciclo:0.60,
    salario_ciclo:400,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "test",
    atividade_ciclo:0.80,
    salario_ciclo:700,
    numero_ciclo_indisponivel:0
)

Dev.create(
    tipo: "test",
    atividade_ciclo:0.9,
    salario_ciclo:1000,
    numero_ciclo_indisponivel:0
)

Recurso.create(
    custo_ciclo: 10
)

Recurso.create(
    custo_ciclo: 10
)

Recurso.create(
    custo_ciclo: 20
)

Recurso.create(
    custo_ciclo: 20
)

Recurso.create(
    custo_ciclo: 30
)

Recurso.create(
    custo_ciclo: 30
)

Recurso.create(
    custo_ciclo: 40
)

Recurso.create(
    custo_ciclo: 40
)

Recurso.create(
    custo_ciclo: 50
)

Recurso.create(
    custo_ciclo: 50
)

Recurso.create(
    custo_ciclo: 60
)

Recurso.create(custo_ciclo: 60)
