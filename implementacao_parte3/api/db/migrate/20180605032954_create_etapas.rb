class CreateEtapas < ActiveRecord::Migration[5.1]
  def change
    create_table :etapas do |t|
      t.boolean :finalizada
      t.string :nome
      t.references :problema, foreign_key: true

      t.timestamps
    end
  end
end
