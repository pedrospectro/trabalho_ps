class CreateCronogramas < ActiveRecord::Migration[5.1]
  def change
    create_table :cronogramas do |t|
      t.integer :numero_ciclos
      t.references :problema, foreign_key: true

      t.timestamps
    end
  end
end
