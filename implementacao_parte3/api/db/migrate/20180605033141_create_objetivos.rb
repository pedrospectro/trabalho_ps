class CreateObjetivos < ActiveRecord::Migration[5.1]
  def change
    create_table :objetivos do |t|
      t.float :custo_etapa
      t.text :especificacao
      t.references :etapa, foreign_key: true

      t.timestamps
    end
  end
end
