class CreateDevs < ActiveRecord::Migration[5.1]
  def change
    create_table :devs do |t|
      t.string :tipo
      t.float :atividade_ciclo
      t.float :salario_ciclo
      t.integer :numero_ciclo_indisponivel

      t.timestamps
    end
  end
end
