class CreateJogadors < ActiveRecord::Migration[5.1]
  def change
    create_table :jogadors do |t|
      t.float :saldo
      t.integer :ciclos_exec

      t.timestamps
    end
  end
end
