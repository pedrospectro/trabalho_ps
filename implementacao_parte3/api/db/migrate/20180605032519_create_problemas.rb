class CreateProblemas < ActiveRecord::Migration[5.1]
  def change
    create_table :problemas do |t|
      t.references :professor, foreign_key: true
      t.text :descricao
      t.integer :ciclo_escolhido_id

      t.timestamps
    end
  end
end
