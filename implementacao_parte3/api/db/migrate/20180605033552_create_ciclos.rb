class CreateCiclos < ActiveRecord::Migration[5.1]
  def change
    create_table :ciclos do |t|
      t.string :tipo
      t.references :problema, foreign_key: false

      t.timestamps
    end
  end
end
