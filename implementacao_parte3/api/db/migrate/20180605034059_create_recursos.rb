class CreateRecursos < ActiveRecord::Migration[5.1]
  def change
    create_table :recursos do |t|
      t.float :custo_ciclo
      t.references :dev, foreign_key: false

      t.timestamps
    end
  end
end
