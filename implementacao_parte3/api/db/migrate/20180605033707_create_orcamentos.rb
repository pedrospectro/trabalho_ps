class CreateOrcamentos < ActiveRecord::Migration[5.1]
  def change
    create_table :orcamentos do |t|
      t.float :valor
      t.references :problema, foreign_key: true

      t.timestamps
    end
  end
end
