class CreateAtividades < ActiveRecord::Migration[5.1]
  def change
    create_table :atividades do |t|
      t.float :status
      t.text :descricao
      t.references :jogador, foreign_key: true
      t.references :etapa, foreign_key: false

      t.timestamps
    end
  end
end
