# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180621043534) do

  create_table "atividades", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float "status", limit: 24
    t.text "descricao"
    t.bigint "jogador_id"
    t.bigint "etapa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etapa_id"], name: "index_atividades_on_etapa_id"
    t.index ["jogador_id"], name: "index_atividades_on_jogador_id"
  end

  create_table "ciclos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "tipo"
    t.bigint "problema_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["problema_id"], name: "index_ciclos_on_problema_id"
  end

  create_table "cronogramas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "numero_ciclos"
    t.bigint "problema_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["problema_id"], name: "index_cronogramas_on_problema_id"
  end

  create_table "devs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "tipo"
    t.float "atividade_ciclo", limit: 24
    t.float "salario_ciclo", limit: 24
    t.integer "numero_ciclo_indisponivel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "contratado", default: false
  end

  create_table "etapas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.boolean "finalizada"
    t.string "nome"
    t.bigint "problema_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["problema_id"], name: "index_etapas_on_problema_id"
  end

  create_table "jogadors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float "saldo", limit: 24
    t.integer "ciclos_exec"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "objetivos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float "custo_etapa", limit: 24
    t.text "especificacao"
    t.bigint "etapa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etapa_id"], name: "index_objetivos_on_etapa_id"
  end

  create_table "orcamentos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float "valor", limit: 24
    t.bigint "problema_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["problema_id"], name: "index_orcamentos_on_problema_id"
  end

  create_table "problemas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "professor_id"
    t.text "descricao"
    t.integer "ciclo_escolhido_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["professor_id"], name: "index_problemas_on_professor_id"
  end

  create_table "professors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recursos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float "custo_ciclo", limit: 24
    t.bigint "dev_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dev_id"], name: "index_recursos_on_dev_id"
  end

  add_foreign_key "atividades", "jogadors"
  add_foreign_key "cronogramas", "problemas"
  add_foreign_key "etapas", "problemas"
  add_foreign_key "objetivos", "etapas"
  add_foreign_key "orcamentos", "problemas"
  add_foreign_key "problemas", "professors"
end
