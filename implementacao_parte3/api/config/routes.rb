Rails.application.routes.draw do
  post '/problema', to: 'problemas#cadastrar_problema'
  post '/etapa', to: 'etapas#cadastrar_etapa'
  post '/etapa/:id/objetivo', to: 'objetivos#cadastrar_objetivo'

  get '/problema', to: 'problemas#get_problema'
  get '/problema/ciclo', to: 'problemas#get_problema_ciclos'
  post '/problema/ciclo', to: 'problemas#escolhe_ciclo'

  get '/jogador', to: 'jogadors#get_saldo'
  post '/problema/cronograma', to: 'cronogramas#montar_cronograma'
  post '/problema/orcamento', to: 'orcamentos#montar_orcamento'

  get '/objetivo/:id', to: 'objetivos#le_objetivo'
  post '/etapa/:id/atividade', to: 'atividades#montar_lista_atividades'

  get '/dev', to: 'devs#get_dev_tipo'
  post '/dev', to: 'devs#contrata_devs'
  get '/dev/contratados', to: 'devs#get_contratados'

  get '/recursos', to: 'recursos#get_recursos'
  post 'dev/:dev_id/recursos/:rec_id', to: 'recursos#aloca_recurso'

  get '/etapa/:id/custo', to: 'etapas#analisar_custo_etapa'
  get '/etapa/:id/calcula_saldo_pos_etapa', to: 'etapas#calcula_saldo_pos_etapa'
  get '/etapa/rodar_primeira_etapa', to: 'etapas#rodar_primeira_etapa'

  post '/atividade/:id/modifica_atividades', to: 'atividades#modifica_atividades'
  post '/dev/:id/aloca_desaloca_recursos', to: 'devs#aloca_desaloca_recursos'
  get '/etapa/:id/verifica_fim_etapa', to: 'etapas#verifica_fim_etapa'
  get '/etapa/rodar_proxima_etapa', to: 'etapas#rodar_proxima_etapa'
end
