ALUNO: Pedro Cavalcante Sa GRR 20094606
PROFESSOR: Andrey Pimentel

Objetivo:
Criar um Projeto de um Software para um determinado problema.
Descri¸c˜ao do Problema:
O trabalho consistir´a em realizar as seguintes atividades:

1. 1a. fase (20% da nota)
• Diagrama de casos de uso
• Casos de uso extendidos
• Modelo de classes conceituais

2. 2a. fase (50% da nota)
• DSS e contratos de opera¸c˜ao
• Diagramas de Interacao
• Diagrama de classes visaao de projeto

3. 3a. fase (30% da nota)
• Completude da implementa¸c˜ao
• Corre¸c˜ao da implementa¸c˜ao
• implementa¸c˜ao do sistema de acordo com o projeto

Cada equipe composta por at´e 4 alunos.

Um Jogo para Engenharia de Software

O Jogo PlanRightOrPerish visa exercitar nos alunos os conhecimentos sobre Planejamento de
Projeto de software. O jogo dever´a poder ser executado em um navegador WEB ou atrav´es de um
aplicativo m´ovel. O Jogo ´e de um Jogador sozinho e deve ter uma interface gr´afica como no exemplo.
O jogo come¸ca quando o jogador recebe a descri¸c˜ao do projeto a ser realizado em termos de caracter´ısticas
que o software dever´a ter. O jogador dever´a estabelecer o modelo de ciclo de vida (com etapas
e fases), estabelecer um cronograma e o or¸camento do projeto.
Para a primeira etapa ser´a mostrado ao jogador os objetivos da etapa. O jogador ir´a criar uma lista
de atividades, criar sua equipe de desenvolvedores, alocar recursos iniciais.
O jogador tem de in´ıcio um capital em BitCruzadosReais (BCZ$) que poder´a usar no seu projeto.
Cada desenvolvedor tem uma capacidade de produ¸c˜ao m´edia dada em atividade/dia e um sal´ario de
acordo com sua capacidade. Cada um dos recursos alocados tera um custo por ciclo de tempo. Cada
desenvolvedor realiza um ´unico papel no projeto (analista, projetista, BD, programador ou testador)
A cada ciclo de tempo podem acontecer os seguintes eventos: Desenvolvedores podem entrar em
licen¸ca por n dias; Desenvolvedores podem pedir demiss˜ao; Desenvolvedores poder estar em treinamento
(metade da capacidade); Desenvolvedores podem ter um dia ruim (70% da capacidade); Computadores e
servidores podem quebrar (1 dia sem produ¸c˜ao); entre outros. A cada ciclo de tempo o jogador poder´a,
modificar a lista de atividades, alocar e desalocar recursos (desenvolvedores e computadores) para cada
uma das atividades da lista.
A cada etapa bem sucedida o jogador recebe o pagamento do cliente. Para a pr´oxima etapa ser´a
mostrado ao jogador os objetivos da etapa. O jogador ir´a criar uma lista de atividades, alocar recursos
iniciais. Quando o cronograma de uma etapa estoura o jogador ´e multado em 5% do or¸camento. O Jogo
termina quando o projeto termina ou o or¸camento estoura.
O professor da disciplina ir´a cadastrar um problema a ser resolvido no jogo com os modelos, os
objetivos a serem completados a cada etapa e o custo de cada objetivo.